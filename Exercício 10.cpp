#include <iostream>

using namespace std;

const int tamanho = 10;

int i;
int maior;
int menor;
int vetor;
int valor;

struct pontos {
    int x;
    int y;
};
struct pontos pont[tamanho];

void escrevavalores (int tam){
    cout <<"Valores de X:" << endl ;
    for(i=0; i < tam; i++){
    cout << "Escreva o valor de X na posicao " << (i+1) << ": ";
    cin >> pont[i].x;
    }
    cout<< endl;
cout <<"Valores de y:" << endl ;
    for(i=0; i < tam; i++){
    cout << "Escreva o valor de Y na posicao " << (i+1) << ": " ;
    cin >> pont[i].y;
    }
    cout<< endl;
}
void maiorX(pontos pont[], int tam){
    for(i=0; i<tam; i++){
        if (pont[i].x > maior){
            maior = pont[i].x;
        }
    }
   cout << "O maior valor de X e: " << maior << endl;
}
void menorY(pontos pont[], int tam){
    for(i=0; i<tam; i++){
        if (pont[i].y < menor){
            menor = pont[i].y;
        }
    }
   cout << "O menor valor de Y e : " << menor << endl;
   cout<< endl;
}
void procurarValores(pontos x[], int tam){
    cout << "Insira o Valor que dever ser procurado nos vetores X e Y:" ;
    cin >> valor;
    cout << endl;
    for(i=0; i<tam; i++){
        if(valor == pont[i].x){
           cout << "As posicoes que apresentam o valor no vetor X:" << i << endl;
        }
    }
    for(i=0; i<tam; i++){
        if(valor == pont[i].y){
            cout << "As posicoes que apresentam o valor no vetor Y:" << i << endl;
        }
    }
}
int main()
{
    escrevavalores(tamanho);
    maiorX(pont, tamanho);
    menorY(pont, tamanho);
    procurarValores(pont, tamanho);
return 0;
}
