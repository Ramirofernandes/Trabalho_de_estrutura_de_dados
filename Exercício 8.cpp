#include <iostream>

using namespace std;
const int tam = 20;
int vetor[tam];

void vertoriniciar(int vetor[], int tam){
    cout<<"Escreve os valores do Vetor"<<endl;
    for(int i = 0; i < tam; i++){
        cout<< "Posicao "<< i+1 << ": ";
        cin>>vetor[i];
    }
    cout<<endl;
}
void imprimirvetor(int vetor[], int tam){
    cout<<"O vertor ordenado e:"<<endl;
    for(int i = 0; i < tam; i++){
        cout<< "Posicao "<< i+1 << ": " <<vetor[i]<<endl;
    }
}
void SelectionSort(int vetor[], int tam){
        for (int i=0; i < tam; i++){
            int menor = i;
            for (int j=i+1; j < tam; j++){
                if (vetor[j] < vetor[menor]){
                    menor = j;
                }
            }
        if (i != menor){
            int temp = vetor[i];
            vetor[i] = vetor[menor];
            vetor[menor] = temp;
        }
    }
    cout<<endl;
}
int main() {
    vertoriniciar(vetor, tam);
    SelectionSort(vetor, tam);
    imprimirvetor(vetor, tam);
    return 0;
}
