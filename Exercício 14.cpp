#include <iostream>

using namespace std;

const int tam=10;
int vetor[tam];

void Vetor(int vetor[],int tam){
    cout << "Digite os valores das posicoes: " <<endl;
    for (int i=0; i < tam; i++){
        cout << "Posicao "<<(i+1)<<": ";
        cin >> vetor[i];
    }
}
void imprimirvetor(int vetor[],int tam){
    cout<<"Vetor de forma ordenada: "<<endl;
    for (int i=0; i<tam; i++){
        cout<<"Posicao "<<(i+1)<<": "<<vetor[i]<<endl;
    }
}
void qs(int vetor[],int esq,int dir);

void quicksort(int vetor[],int esq,int dir){
    qs(vetor,esq,dir-1);
}

void qs(int vetor[],int esq,int dir){
    int i,j;
    i=esq;
    j=dir;
    int meio = vetor[(esq+dir)/2];
   while (j>i){
        while (vetor[i]<meio && i<dir) i++;
        while (meio<vetor[j] && j>esq) j--;

        if (i<=j){
            int temp=vetor[i];
            vetor[i]=vetor[j];
            vetor[j]=temp;
            i++;
            j--;
        }
    }
    if (esq<j) qs(vetor,esq,j);
    if (i<dir) qs(vetor,i,dir);

}
int main(){
    Vetor(vetor,tam);
    quicksort(vetor,0,tam);
    imprimirvetor(vetor,tam);
    return 0;
}
